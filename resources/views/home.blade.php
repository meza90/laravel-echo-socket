@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @auth
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Estas logueado!
                </div>
            </div>
            @else

            <div class="well">
                Por favor <a href="{{ url('login') }}">Inicia sesión</a>
            </div>

            @endauth
        </div>
    </div>
</div>
@endsection
