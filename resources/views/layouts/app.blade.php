<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-notifications.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script type="text/javascript">
        var url = "{{ url('/') }}";
        @auth
window.Laravel = @json(array('id' => auth()->id(), 'user' => auth()->user()->name));
        @endauth
    </script>
    @stack('css')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ route('index') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('messages') }}">Mensajes</a></li>
                        <li><a href="{{ url('posts') }}">Posts</a></li>
                        <li><a href="{{ url('grid') }}">Grids</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                    @auth
                      <li class="dropdown dropdown-notifications">
                        @php $count = $notifications->where('read_at', NULL)->count(); @endphp
                        <a href="#notifications-panel" class="dropdown-toggle" data-toggle="dropdown">
                          <i data-count="{{ $notifications->where('read_at', NULL)->count() }}" class="glyphicon glyphicon-bell {{ $count ? 'notification-icon' : '' }}"></i>
                        </a>

                        <div class="dropdown-container dropdown-position-bottomright">
                          <div class="dropdown-toolbar">
                            <div class="dropdown-toolbar-actions">
                              <a href="{{ url('notifications/read-all') }}">Mark all as read</a>
                            </div>
                            <h3 class="dropdown-toolbar-title">Notifications ({{ $notifications->count() }})</h3>
                          </div><!-- /dropdown-toolbar -->

                          <ul class="dropdown-menu" id="notification-menu">
                            @foreach ($notifications as $notification)
                              @include('layouts.notifications')
                            @endforeach
                          </ul>
                          <div class="dropdown-footer text-center">
                            <a href="#">View All</a>
                          </div><!-- /dropdown-footer -->
                        </div><!-- /dropdown-container -->
                      </li><!-- /dropdown -->
                    @endauth

                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ auth()->id() }} - {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.slim.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{!! mix('js/app.js') !!}"></script>
    @stack('js')
</body>
</html>
