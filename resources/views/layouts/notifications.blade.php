<li class="notification {{ $notification->read_at ? '' : 'active' }}">
    <div class="media">
      <div class="media-left">
        <div class="media-object"></div>
      </div>
      <div class="media-body">
        <strong class="notification-title"><a href="{{ url('users', [$notification->data['user_id']]) }}">{{ $notification->data['name'] }}</a> comentó tu post <a href="{{ url('posts', $notification->data['post_id']) }}">{{ $notification->data['post_name'] }}</a></strong>
        <div class="notification-meta">
          <small class="timestamp">{{ $notification->created_at->diffForHumans() }}</small>
        </div>
      </div>
    </div>
</li>