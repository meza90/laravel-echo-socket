<div class="panel panel-primary">
	<div class="panel-heading">
		Chat en grupo
	</div>

	<div class="panel-body">
		<ul id="post-chat" style="height: 200px; list-style: none; overflow-y: scroll;">
            @foreach ($chats as $chat)
                <li>
                    <strong>{{ $chat->user->name }}</strong>
                    {{ $chat->body }}
                </li>
            @endforeach
        </ul>
	</div>

    <div class="panel-footer">
        <form method="POST" action="{{ route('posts.chat', [$post->id]) }}" id="post-form-chat">
            {{ csrf_field() }}
            <div class="input-group">
                <input type="text" name="body" id="body" autocomplete="off" class="form-control input-sm">
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-default btn-sm">Enviar</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        Usuarios en este grupo
    </div>

    <div class="panel-body">
        <ul id="post-users-online" style="height: 150px; list-style: none; overflow-y: scroll;">
        </ul>
    </div>
</div>