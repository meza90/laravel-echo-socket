@foreach ($post->comments as $comment)
	<div class="media" id="{{ $comment->id }}">
	    <a class="pull-left" href="{{ url('users', [$comment->user->id]) }}">
	        <img class="media-object" src="http://placehold.it/64x64" alt="">
	    </a>

		<div class="media-body">
		    <h4 class="media-heading">
		    	{{ $comment->user->name }}
		        <small>
		        	{{ $comment->created_at->diffForHumans() }}
		        </small>
		    </h4>

			<span class="pull-right like-count">{{ $comment->hands_count > 0 ?: '' }}</span>

		    {{ $comment->body }}

		    <p>
		    	<a href="#" class="btn-like" data-id="{{ $comment->id }}" title="Me gusta">
		    		{{ in_array($comment->id, $user) ? 'Te gusta' : 'Me gusta' }}
		    	</a>
		    </p>
		</div>
	</div>
@endforeach