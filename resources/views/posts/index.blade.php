@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="page-header">
          <h1>Tus Posts <small>({{ $posts->count() }})</small></h1>
        </div>
        <div class="col-md-10">
            @foreach ($posts as $post)
                <div class="well" style="margin-bottom: 10px">
                    <a href="{{ url('posts', [$post->id]) }}">
                        {{ $post->body }}
                    </a>
                    <div class="pull-right">
                        {{ $post->category->name }}
                        <i class="fa fa-clock-o"></i> {{ $post->created_at->diffForHumans() }}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
