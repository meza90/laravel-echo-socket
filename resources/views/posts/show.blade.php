@extends('layouts.app')

@push('css')
<script type="text/javascript">
	window.App = @json(['postId' => $post->id])
</script>
@endpush

@section('content')

<div class="container">
    <div class="row">
		<div class="col-lg-8">
		    <h1>
		    	{{ $post->body }}
		    </h1>
		    <p class="lead">
		        Por <a href="{{ route('users.show', [$post->user->id]) }}">
		        		{{ $post->user->name }}
		        	</a>

		    </p>

			<small>
				{{ $visits }} {{ str_plural('visita', $visits) }}
			</small>

		    <hr>

		    <p>
		    	<span class="glyphicon glyphicon-time"></span> Posted on {{ $post->created_at->diffForHumans() }}
		    	@if ($post->user_id === auth()->id())
		    		<form action="{{ url('posts', $post->id) }}" method="POST">
		    			{{ csrf_field() }}
		    			{{ method_field('DELETE') }}
		    			<button type="submit" class="btn btn-danger">Borrar</button>
		    		</form>
		    	@endif
		    </p>

			<form class="pull-right" method="POST" action="{{ route('likes.posts', [$post->id]) }}">
				{{ csrf_field() }}
				@auth
					@if (auth()->user()->hands()->getClass('App\Post')->where('handable_id', $post->id)
							->exists())
						<button type="submit" disabled class="btn btn-default btn-sm">Guardado</button>
					@else
						<button type="submit" class="btn btn-primary btn-sm">Guardar</button>
					@endif
				@endauth
			</form>

		    <hr>

		    <img class="img-responsive" src="http://placehold.it/900x300" alt="">

		    <hr>

		    <p class="lead">
		    	{{ $post->body }}
		    </p>

		    <hr>

		    <div class="well">
		        <h4>Leave a Comment:</h4>
		        <form role="form" method="POST" action="{{ url('comments', [$post->id]) }}">
		        	{{ csrf_field() }}
		            <div class="form-group">
		                <textarea class="form-control" rows="3" name="body"></textarea>
		            </div>
		            <button type="submit" class="btn btn-primary">Submit</button>
		        </form>
		    </div>

		    <hr>

		    @include('posts.comments')
		</div>
		<div class="col-lg-4">
			@include('posts.chat')
		</div>
    </div>
</div>

<hr>

@endsection

@push('js')
<script src="{!! asset('js/notifications.js') !!}"></script>
@endpush