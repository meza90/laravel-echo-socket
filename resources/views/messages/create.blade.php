@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Chateando con {{ $user->name }}</div>

                <div class="panel-body">
                    <ul id="chat" style="height: 300px; list-style: none; overflow-y: scroll;"></ul>
                </div>
                <div class="panel-footer">
                    <form action="{{ url('messages', [$user->id]) }}" method="POST" id="form-chat">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" name="body" autocomplete="off" class="form-control input-sm">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default btn-sm">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')

<script type="text/javascript">
$(document).ready(function() {
    Echo.private('chat-users.{{ auth()->id() }}.{{ $user->id }}')
        .listen('ChatEvent', (e) => {
           $('#chat').append(`<li><strong>${e.body}</strong></li>`);
            var elem = document.getElementById('chat');
            elem.scrollTop = elem.scrollHeight;
        });

    $('#form-chat').on('submit', function(event) {
        event.preventDefault();
        var form = $(this);
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            dataType: 'JSON',
            data: form.serialize(),
        })
        .done(function(response) {

            $('#chat').append(`<li>${response.body}</li>`);
            form.find('input[name=body]').val('').focus();

            var elem = document.getElementById('chat');
            elem.scrollTop = elem.scrollHeight;
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    });
});
</script>

@endpush
