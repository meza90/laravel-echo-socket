@extends('layouts.app')

@push('css')
<style type="text/css">
	#table-index td
	{
		cursor: pointer;
	}
</style>
@endpush

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
			  <h1>Grid realtime <small>pruebas</small></h1>
			</div>

			<table class="table table-condensed table-bordered" id="table-index">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Email</th>
						<th>Creado</th>
					</tr>
				</thead>
				<tbody>
					@foreach (\App\User::take(10)->get() as $user)
					<tr>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->created_at->diffForHumans() }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection

@push('js')

<script type="text/javascript">

	Echo.channel(`click-cell-channel`)
	  	.listen('CellActiveEvent', (e) => {
	    	$('tbody tr:eq('+e.rowIndex+') td:eq('+e.columnIndex+')').css('backgroundColor', '#92B558');
		});

	$('td').click(function(){

		var row = $(this).parent().index();
		var column = $(this).index();
		var data = {row: row, column: column};

		$.ajax({
			url: 'grid',
			type: 'GET',
			data: data,
		})
		.done(function() {
			$(this).css('backgroundColor', '#000000');
		});

	});

</script>

@endpush