
var user = Laravel.id;

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

//Notification de Comentario en post
Echo.private(`notificar.${user}`)
  .listen('CommentEvent', (e) => {

    var audio = new Audio(`${url}/sounds/notification.mp3`);
    audio.play();

    var notificationsWrapper     = $('.dropdown-notifications');
    var notificationsCountElement = notificationsWrapper.find('a').find('i');
    var notificationsCount        = parseInt(notificationsCountElement.attr('data-count')) || 0;
    var notificationsTotal        = notificationsCount + 1;

    if (notificationsCount === 0) notificationsCountElement.addClass('notification-icon');

    notificationsCountElement.attr('data-count', notificationsTotal);

    var template = `
        <li class="notification active">
            <div class="media">
              <div class="media-left">
                <div class="media-object"></div>
              </div>
              <div class="media-body">
                <strong class="notification-title"><a href="${url}/users/${e.user}">${e.name}</a> comentó tu <a href="/posts/${e.post_id}">${e.post_name}</a></strong>

                <p class="notification-desc">Resolution: Fixed, Work log: 4h</p>

                <div class="notification-meta">
                  <small class="timestamp">${e.date}</small>
                </div>
              </div>
            </div>
        </li>`;

    $('#notification-menu').prepend(template);

    toastr["success"]("Nueva notificación");

  });

//Notification de like en comentario
Echo.private(`like-notify.${user}`)
  .listen('LikeEvent', (e) => {
    console.log(e);
     var template = `
      <li class="notification active">
          <div class="media">
            <div class="media-left">
              <div class="media-object"></div>
            </div>
            <div class="media-body">
              <strong class="notification-title">A <a href="${url}/users/${e.user.id}">${e.user.name}</a> le gustó tu <a href="${url}/posts/${e.comment.post_id}#${e.comment.id}">comentario</a></strong>

              <p class="notification-desc">Resolution: Fixed, Work log: 4h</p>

              <div class="notification-meta">
                <small class="timestamp">27. 10. 2015, 08:00</small>
              </div>
            </div>
          </div>
      </li>`;

     $('#notification-menu').prepend(template);
});