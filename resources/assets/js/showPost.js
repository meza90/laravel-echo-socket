$('#post-form-chat').on('submit', function(event) {
    event.preventDefault();
    var form = $(this);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        dataType: 'JSON',
        data: form.serialize(),
    })
    .done(function() {
        form.find('#body').val('').focus();
        var elem        = document.getElementById('post-chat');
        elem.scrollTop  = elem.scrollHeight;
    })
});

$('.btn-like').on('click', function(event) {
    event.preventDefault();
    var element         = $(this);
    var comment         = element.data('id');
    var likesSelector   = element.parent('p').prev('.like-count');
    var likesCount      = parseInt(likesSelector.text()) || 0;
    $.ajax({
        url: `${url}/likes/${comment}/comments`,
        type: 'POST',
        dataType: 'JSON',
    }).done(function(data) {
        event.target.blur();
        if(data.action === 'new'){
            likesSelector.text(likesCount += 1);
            event.target.innerText = 'Te gusta';
        }
        else {
            event.target.innerText = 'Me gusta';
            if(--likesCount === 0) likesSelector.text('');
            else likesSelector.text(--likesCount);
        }
    })
});