var post = App.postId;
var usersOnlineSelector = $('#post-users-online');

//Chat en posts
Echo.join(`post-chat-${post}`)
  .here((users) => {
    var html = "";

      $.each(users, function(index, value) {
        html += `<a href="${url}/users/${value.id}"><li id=${value.id}>${value.name}</li></a>`;
      });

      usersOnlineSelector.append(html);
  })
  .joining((user) => {
      usersOnlineSelector.append(
        `<a href="${url}/users/${user.id}"><li id=${user.id}>${user.name}</li></a>`
      );
  })
  .leaving((user) => {
      usersOnlineSelector.find(`li#${user.id}`).remove();
  })
  .listen('ChatPostEvent', (e) => {
    $('#post-chat').append(`
          <li>
            <strong>${e.name }</strong>
              ${e.body}
          </li>`
      );
      var elem = document.getElementById('post-chat');
      elem.scrollTop = elem.scrollHeight;
  });