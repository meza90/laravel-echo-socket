<?php

Route::get('getMorph', function(){
	return App\Hand::with('handable')->get();
});

Route::get('collection', function(\App\Comment $comment){
    return App\User::whereIn('id', [1, 2, 3])
        ->get()
        ->each(function ($user) {
            $user->updateName();
        });
});

#Disparando el JOB, usamos directamente EL JOB SendEmailJob
Route::get('/job', function(){
	dispatch(new App\Jobs\SendEmailJob());
	return redirect()->to('/home');
});

#Usamos el evento, que implementa ShouldQueue e InteractsWithQueue para los Queues
Route::get('eventoConQueue', function(){
	$post = App\Post::first();
	event(new App\Events\NotifyEmailEvent($post));
	return redirect()->to('home');
});
