<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('logout', function(){
	auth()->logout();

	return redirect()->to('/');
})->name('logout');

Route::post('/loginWithId', function(){
	Auth::loginUsingId(request()->id);
	return redirect()->to('/');
});

Auth::routes();
Route::get('/', 'HomeController@index')->name('index');
Route::get('grid', 'ActiveCellController@ajax');

#pusher
Route::middleware('auth')->group(function(){
	Route::get('messages', 'MessageController@index');
	Route::get('messages/{user}', 'MessageController@create');
	Route::post('messages/{user}', 'MessageController@store');
	Route::post('comments/{post}', 'CommentController@store');
	Route::post('likes/{comment}/comments', 'LikeController@comment');
	Route::post('likes/{post}/posts', 'LikeController@post')->name('likes.posts');

	Route::get('notifications/read-all', function(){
		auth()->user()->unreadNotifications->markAsRead();
		return back();
	});

	Route::resource('posts', 'PostController', ['parameters' => 'post']);
	Route::resource('users', 'UserController', ['parameters' => 'user']);
	Route::post('posts/{post}/chats', 'ChatController@store')->name('posts.chat');

});

require __DIR__ . '/others.php';
