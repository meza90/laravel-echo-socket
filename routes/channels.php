<?php

use App\Post;
use App\User;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('chat-users.{transmitter}.{receiver}', function ($transmitter, $receiver) {
    return true;
});

Broadcast::channel('notificar.{user}', function ($user) {
	return true;
});

Broadcast::channel('like-notify.{user}', function ($user) {
	return true;
});

Broadcast::channel('post-chat-{postId}', function ($user, $postId) {
	return ['id' => $user->id, 'name' => $user->name];
});