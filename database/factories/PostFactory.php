<?php

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 8500),
        'category_id' => rand(1, 500),
        'body' => $faker->text($maxNbChars = 100)
    ];
});
