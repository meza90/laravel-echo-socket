<?php

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 8500),
        'post_id' => rand(1, 50000),
        'body' => $faker->text($maxNbChars = 100)
    ];
});
