<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory('App\User', 8500)->create();
    	factory('App\Category', 500)->create();
    	factory('App\Post', 50000)->create();
    }
}
