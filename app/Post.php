<?php

namespace App;

use App\Notifications\UserComment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;

class Post extends Model
{
	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

    public function comments()
    {
        return $this->hasMany(Comment::class)->orderByDesc('created_at');
    }

    public function hands()
    {
    	return $this->morphMany(Hand::class, 'handable');
    }

    public function chats()
    {
    	return $this->hasMany(Chat::class);
    }

    public function notifyPost($post)
    {
        Notification::send($this->user, new UserComment($post, auth()->user()));
    }
}
