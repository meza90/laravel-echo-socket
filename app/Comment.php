<?php

namespace App;

use App\Notifications\UserComment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;

class Comment extends Model
{

	protected $table = 'post_user';

	protected $fillable = ['body', 'user_id', 'post_id'];

    public function hands()
    {
    	return $this->morphMany(Hand::class, 'handable');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
