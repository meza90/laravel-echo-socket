<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hand extends Model
{
	protected $fillable = ['user_id'];

    public function handable()
    {
    	return $this->morphTo();
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function scopeGetUser($q)
    {
        return $q->where('user_id', auth()->id());
    }

    public function scopeGetClass($query, $type)
    {
    	return $query->where('handable_type', $type);
    }
}
