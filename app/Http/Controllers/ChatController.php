<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Events\ChatPostEvent;
use App\Post;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $chat = $post->chats()->create([
            'user_id' => auth()->id(),
            'body' => $request->body
        ]);

        broadcast(new ChatPostEvent($post->id, auth()->user()->name, $request->body)); //->toOthers();

        return response()->json(array('message' => 'OK'), 200);
    }
}
