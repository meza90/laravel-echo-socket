<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Hand;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = auth()->user()->posts()->with('category')->get();

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Hand::getClass('App\Comment')
                    ->getUser()
                    ->pluck('handable_id')
                    ->toArray();

        $post = Post::with('comments.user')
                ->with(['comments' => function($q){
                        $q->withCount('hands');
                }])->where('id', $id)->firstOrFail();

        $chats = Chat::select('body', 'created_at', 'user_id')
                ->with('user')
                ->where('post_id', $post->id)
                ->orderBy('created_at')
                ->get();

        // $visits = Redis::incr("post_visits_{$post->id}");
        $visits = 1;

        return view('posts.show', compact('user', 'post', 'chats', 'visits'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->comments()->delete();

        $post->chats()->delete();

        $post->hands()->delete();

        $post->delete();

        return redirect()->to('posts');
    }
}
