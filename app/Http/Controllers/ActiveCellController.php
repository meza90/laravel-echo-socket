<?php

namespace App\Http\Controllers;

use App\Events\CellActiveEvent;
use Illuminate\Http\Request;

class ActiveCellController extends Controller
{
    public function ajax(Request $request)
    {
    	if($request->ajax()) {

	    	$row = $request->row;

	    	$column = $request->column;

	        broadcast(new CellActiveEvent(
	        	$row,
	        	$column
	        ));

    	}

    	return view('grid');
    }
}
