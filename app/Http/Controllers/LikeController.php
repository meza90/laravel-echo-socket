<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    /**
     * Store a newly like comment in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function comment(Request $request, Comment $comment)
    {
        if($request->user()->comment($comment))
            return response()->json(array('action' => 'new'));

        return response()->json(array('action' => 'delete'));
    }

    /**
     * Store a newly like post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post(Request $request, Post $post)
    {
        $post->hands()->create(['user_id' => auth()->id()]);

        return back();
    }
}
