<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['transmitter_id', 'receiver_id', 'body'];
}
