<?php

namespace App;

use App\Comment;
use App\Events\LikeEvent;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relacion uno a muchos con posts
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function updateName()
    {
        static::update(['name' => $this->id.' '.Str::random(10)]);
    }

    /*
        Relación uno a muchos con likes
     */
    public function hands()
    {
        return $this->hasMany(Hand::class);
    }

    /*
        Relacion uno a muchos con comentarios
     */
    public function comments()
    {
        return $this->hasMany(Comment::class, 'user_id');
    }

    /*
        Reutilizar funcion para saber si un like existe
     */
    public function like($id)
    {
        return $this->hands()->getClass('App\Comment')->where('handable_id', $id);
    }

    /*
        Borrar un like ya guardado o guardar un nuevo like y disparar
        el broadcast
     */
    public function comment(Comment $comment)
    {
        if($this->like($comment->id)->exists()){

            $this->like($comment->id)->delete();

            return false;
        }

        $comment->hands()->create(array('user_id' => $this->id));

        if($comment->user_id !== $this->id) broadcast(new LikeEvent(auth()->user(), $comment));

        return true;

    }

    public function likes()
    {
        return $this->belongsToMany(Like::class, 'likes', 'user_id', 'post_id')
                    ->withTimestamps();
    }

    public function chats()
    {
        return $this->hasMany(Chat::class);
    }
}
